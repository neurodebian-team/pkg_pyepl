# PyEPL: tests/soundTest.py
#
# Copyright (C) 2003-2005 Michael J. Kahana
# Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
# URL: http://memory.psych.upenn.edu/programming/pyepl
#
# Distributed under the terms of the GNU Lesser General Public License
# (LGPL). See the license.txt that came with this file.

#!/usr/bin/python

# this module tests sound functionality
# it assumes pyepl has been installed for the scoping to be correct.

from pyepl.locals import *

exp = Experiment()
exp.parseArgs()
exp.setup()

audio = AudioTrack("audio")
video = VideoTrack("video")

beepdur = 500
numSecs = 5
recdur = 1000*numSecs
reps = 3

clk = PresentationClock()
video.clear("black")
video.updateScreen(clk)

# make a beep
beep1 = Beep(400, beepdur, 100)
beep1.present(clk)

# start recording...
(notsnd, starttime) = audio.startRecording(None, clk.get())

for i in range(numSecs):
    t = video.showCentered(Text("Recording: %d" % (i+1),color=(1,0,0)))
    video.updateScreen(clk)
    clk.delay(1000)
    video.unshow(t)
    video.updateScreen(clk)

# stop recording
(snd, stoptime) = audio.stopRecording(t=clk.get())

for r in range(reps):
    # beep
    beep1.present(clk)

    # play sound, don't increment the clock though
    audio.play(snd, clk, doDelay=False) 

    for i in range(numSecs):
	t = video.showCentered(Text("Rep %d, Playing: %d" % (r+1, numSecs-i)))
	video.updateScreen(clk)
	clk.delay(1000)
	video.unshow(t)
	video.updateScreen(clk)

clk.delay(1000)
clk.wait()
