# PyEPL: hardware/vr/environment.pyx
#
# Copyright (C) 2003-2005 Michael J. Kahana
# Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
# URL: http://memory.psych.upenn.edu/programming/pyepl
#
# Distributed under the terms of the GNU Lesser General Public License
# (LGPL). See the license.txt that came with this file.

from OpenGL.GL import glGenLists, glNewList, glEndList, glEnable, glDisable, glViewport, glMatrixMode, glLoadIdentity, glClear, glRotatef, glTranslatef, glCallLists, glBindTexture, glBegin, glTexCoord2f, glVertex3f, glEnd, GL_COMPILE, GL_TEXTURE_2D, GL_PROJECTION, GL_MODELVIEW, GL_DEPTH_BUFFER_BIT, GL_QUADS, glCallList, GL_DEPTH_TEST, GL_TRUE, glPushMatrix, glPopMatrix, glDeleteLists, GL_CULL_FACE, glCullFace, GL_FRONT, glGetFloatv, GL_MODELVIEW_MATRIX, GL_TRIANGLE_STRIP, glVertex2f, GL_BLEND, glBlendFunc, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, glFogi, glFogf, glFogfv, GL_FOG, GL_FOG_MODE, GL_FOG_DENSITY, GL_FOG_START, GL_FOG_END, GL_FOG_COLOR, GL_LINEAR, GL_EXP, GL_EXP2, glHint, GL_FOG_HINT, GL_NICEST, glColor4f, glTexParameteri, GL_TEXTURE_WRAP_S
from OpenGL.GLU import gluPerspective, gluSphere, gluNewQuadric, gluQuadricNormals, gluQuadricTexture, GLU_SMOOTH, gluQuadricOrientation, GLU_INSIDE

import ode
from pyepl.hardware.timing import universal_time
from numpy import reshape, asarray
from math import sqrt, ceil

cdef object collattrs
cdef float maxfacetlength

# OSX glLoadMatrixf fix
import OpenGL.GL
cdef object glLoadMatrixf
if hasattr(OpenGL.GL,"glLoadMatrixf"):
    glLoadMatrixf = OpenGL.GL.glLoadMatrixf
else:
    glLoadMatrixf = OpenGL.GL.glLoadMatrix
    
collattrs = [
    ("mu", "setMu", 0),
    ("mu2", "setMu2", ode.ContactMu2),
    ("bounce", "setBounce", ode.ContactBounce),
    ("bounce_vel", "setBounceVel", ode.ContactBounce),
    ("soft_erp", "setSoftERP", ode.ContactSoftERP),
    ("soft_cfm", "setSoftCFM", ode.ContactSoftCFM),
    ("motion1", "setMotion1", ode.ContactMotion1),
    ("motion2", "setMotion2", ode.ContactMotion2),
    ("slip1", "setSlip1", ode.ContactSlip1),
    ("slip2", "setSlip2", ode.ContactSlip2),
    ("contact_approx1_1", None, ode.ContactApprox1_1),
    ("contact_approx1_2", None, ode.ContactApprox1_2),
    ("contact_approx1", None, ode.ContactApprox1)
    ]

def setMaxFacetLength(float x):
    global maxfacetlength
    maxfacetlength = x

cdef class VREntity:
    pass

cdef class VRShape(VREntity):
    def getODEGeometries(self, world, space):  # To be overridden
        pass

cdef class VRVisible(VREntity):
    def prepare(self):  # To be overridden
        pass
    def construct(self):  # To be overridden
        pass
    def clean(self):  # To be overridden
        pass

cdef class VRDynamic(VREntity):
    def liveConstruct(self, float x, float y, float z, float yaw, float pitch, float roll, float fovy, float aspect, int width, int height):  # To be overridden
        pass

cdef class LowVEnvironment:
    cdef object entities
    cdef object gl_lists
    cdef object gl_live_constructs
    cdef object ode_geometries
    cdef readonly object world
    cdef object contactgroup
    cdef readonly object space
    cdef object ode_to_remove
    cdef object ode_to_add
    cdef object laststep
    cdef int stepping
    cdef int fogmode
    cdef object fogcolor
    cdef float fogfar
    cdef float fognear
    cdef float fogdensity
    def __init__(self):
        """
        """
        self.entities = {}
        self.gl_lists = []
        self.gl_live_constructs = []
        self.fogmode = 0
        
        # create ODE world...
        self.world = ode.World()
        self.contactgroup = ode.JointGroup()
        self.space = ode.Space()
        self.ode_to_remove = []
        self.ode_to_add = []
        self.laststep = None
        self.stepping = 0
    def __del__(self):
        """
        """
        for entity in self.entities.keys():
            self.removeEntity(entity)
    def addEntity(self, entity):
        """
        """
        if isinstance(entity, VRShape):
            self.ode_to_add.append(entity)
        geoms = []
        if isinstance(entity, VRVisible):
            listnum = glGenLists(1)
            entity.prepare()
            glNewList(listnum, GL_COMPILE)
            entity.construct()
            glEndList()
        else:
            listnum = None
        if isinstance(entity, VRDynamic):
            self.gl_live_constructs.append(entity)
        self.entities[entity] = (listnum, geoms)
        if not listnum is None:
            self.gl_lists.append(listnum)
        return entity
    def removeEntity(self, entity):
        """
        """
        listnum, geoms = self.entities.pop(entity)
        if not listnum is None:
            glDeleteLists(listnum, 1)
            self.gl_lists.remove(listnum)
        for geom in geoms:
            self.ode_to_remove.append(geom)
        if entity in self.gl_live_constructs:
            self.gl_live_constructs.remove(entity)
    def render(self, float x, float y, float z, float yaw, float pitch, float roll, float fovy, float aspect, float zNear, float zFar, int lowerleftx, int lowerlefty, int width, int height):
        """
        """
        cdef object i

        glEnable(GL_TEXTURE_2D)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_CULL_FACE)
        #glCullFace(GL_FRONT)
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        if self.fogmode:
            glFogi(GL_FOG_MODE, self.fogmode)
            glFogf(GL_FOG_DENSITY, self.fogdensity)
            glFogf(GL_FOG_START, self.fognear)
            glFogf(GL_FOG_END, self.fogfar)
            glFogfv(GL_FOG_COLOR, self.fogcolor)
            glHint(GL_FOG_HINT, GL_NICEST)
            glEnable(GL_FOG)
        else:
            glDisable(GL_FOG)
        glViewport(lowerleftx, lowerlefty, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(fovy, aspect, zNear, zFar)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glClear(GL_DEPTH_BUFFER_BIT)

        glRotatef(yaw, 0.0, -1.0, 0.0)
        glRotatef(pitch, -1.0, 0.0, 0.0)
        glRotatef(roll, 0.0, 0.0, -1.0)
        glTranslatef(-x, -y, -z)
        #for i in self.gl_lists:
        #    glCallList(i)
        glCallLists(self.gl_lists)
        for i in self.gl_live_constructs:
            i.liveConstruct(x, y, z, yaw, pitch, roll, fovy, aspect, width, height)
    def near_callback(self, args, geom1, geom2):
        """
        """
        global collattrs
        cdef object contacts
        cdef object world
        cdef object contactgroup
        cdef long mode
        cdef object x
        cdef object attrname
        cdef object setfuncname
        cdef long modebit
        contacts=ode.collide(geom1,geom2)

        if not len(contacts):
            return

        # Callbacks:
        try:
            if hasattr(geom1, "callback"):
                try:
                    cbargs = geom1.cbargs
                except AttributeError:
                    cbargs = ()
                geom1.callback(*cbargs)
            if hasattr(geom2, "callback"):
                try:
                    cbargs = geom2.cbargs
                except AttributeError:
                    cbargs = ()
                geom2.callback(*cbargs)
        except:
            import traceback
            traceback.print_exc()

        if (hasattr(geom1, "permeable") and geom1.permeable) or (hasattr(geom2, "permeable") and geom2.permeable):
            return

        #create the contact joints
        world, contactgroup = args
        for c in contacts:
            mode = 0
            for attrname, setfuncname, modebit in collattrs:
                try:
                    x = getattr(geom1, attrname)
                    mode = mode | modebit
                    if setfuncname:
                        getattr(c, setfuncname)(x)
                except AttributeError:
                    try:
                        x = getattr(geom2, attrname)
                        mode = mode | modebit
                        if setfuncname:
                            getattr(c, setfuncname)(x)
                    except AttributeError:
                        pass
            c.setMode(mode)
            j=ode.ContactJoint(world, contactgroup, c)
            j.attach(geom1.getBody(), geom2.getBody())
    cdef updateGeoms(self):
        cdef object geoms
        cdef object geom
        cdef object x
        for geom in self.ode_to_remove:
            # this is ugly but necesssary if we're going to use
            # plain PyODE
            for key in ode._geom_c2py_lut.keys():
                if ode._geom_c2py_lut[key]==geom:	        
                    del ode._geom_c2py_lut[key]
        for x in self.ode_to_add:
            geoms = x.getODEGeometries(self.world, self.space)
            self.entities[x] = (self.entities[x][0], geoms)
        self.ode_to_remove = []
        self.ode_to_add = []
    def dynamicStep(self, float substep):
        """
        Update the dynamics simulation by the amount of time specified.
        """
        cdef int x
        cdef object steptime
        cdef float milliseconds
        if self.stepping:
            return
        self.stepping = 1
        self.updateGeoms()
        steptime = universal_time()
        if self.laststep:
            milliseconds = steptime - self.laststep
        else:
            milliseconds = 0.0
            self.laststep = steptime
        if milliseconds:
            self.laststep = steptime
            while milliseconds > 0.0:
                self.space.collide((self.world, self.contactgroup), self.near_callback)
                self.updateGeoms()
                self.world.step(min(milliseconds, substep))
                self.contactgroup.empty()
                milliseconds = milliseconds - substep
        self.stepping = 0
    def setGravity(self, x, y, z):
        """
        """
        self.world.setGravity((x, y, z))
    def setFog(self, mode = None, color = (0.5, 0.5, 0.5), far = 1.0, near = 0.0, density = 1.0):
        if mode is None:
            mode = 0
        elif mode == "linear":
            mode = GL_LINEAR
        elif mode == "exponential":
            mode = GL_EXP
        elif mode == "exponential^2":
            mode = GL_EXP2
        else:
            raise ValueError, "Invalid fog mode: %r" % mode
        self.fogmode = mode
        self.fogcolor = color
        self.fogfar = far
        self.fognear = near
        self.fogdensity = density

cdef class SphereGeom(VRShape):
    """
    """
    cdef float x
    cdef float y
    cdef float z
    cdef float radius
    cdef object surface_options
    def __init__(self, x, y, z, radius, **surface_options):
        """
        """
        self.x = x
        self.y = y
        self.z = z
        self.radius = radius
        self.surface_options = surface_options
    def getODEGeometries(self, world, space):
        """
        """
        #make a geom box for collision detection
        geom=ode.GeomSphere(space, self.radius)
        geom.setBody(None)
        
        geom.setPosition((self.x, self.y, self.z))
        geom.setCategoryBits(0x00000001)
        geom.setCollideBits(long('0xFFFFFFFE', 16))
        
        for key, value in self.surface_options.iteritems():
            setattr(geom, key, value)
        return (geom,)

cdef class BoxGeom(VRShape):
    """
    """
    cdef float x
    cdef float y
    cdef float z
    cdef float xsize
    cdef float ysize
    cdef float zsize
    cdef object surface_options
    def __init__(self, x, y, z, xsize, ysize, zsize, **surface_options):
        """
        """
        self.x = x
        self.y = y
        self.z = z
        self.xsize = xsize
        self.ysize = ysize
        self.zsize = zsize
        self.surface_options = surface_options
    def getODEGeometries(self, world, space):
        """
        """
        #make a geom box for collision detection
        geom=ode.GeomBox(space, (self.xsize, self.ysize, self.zsize))
        geom.setBody(None)
        
        geom.setPosition((self.x, self.y, self.z))
        geom.setCategoryBits(0x00000001)
        geom.setCollideBits(long('0xFFFFFFFE', 16))
        
        for key, value in self.surface_options.iteritems():
            setattr(geom, key, value)
        return (geom,)

cdef class PlaneGeom(VRShape):
    """
    """
    cdef float a
    cdef float b
    cdef float c
    cdef float d
    cdef object surface_options
    def __init__(self, a, b, c, d, **surface_options):
        """
        """
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.surface_options = surface_options
    def getODEGeometries(self, world, space):
        """
        """
        #make a geom box for collision detection
        geom=ode.GeomPlane(space, (self.a, self.b, self.c), self.d)

        geom.setCategoryBits(0x00000001)
        geom.setCollideBits(long('0xFFFFFFFE', 16))
        
        for key, value in self.surface_options.iteritems():
            setattr(geom, key, value)
        return (geom,)

cdef class Sphere(VRVisible):
    """
    """
    cdef object image
    cdef float radius
    cdef int slices
    cdef int stacks
    cdef float x
    cdef float y
    cdef float z
    def __init__(self, x, y, z, image, radius, slices = 16, stacks = 16):
        self.image = image
        self.radius = radius
        self.slices = slices
        self.stacks = stacks
        self.x = x
        self.y = y
        self.z = z
    def construct(self):
        cdef object quad
        quad = gluNewQuadric()
        gluQuadricNormals(quad, GLU_SMOOTH)
        gluQuadricTexture(quad, GL_TRUE)
        gluQuadricOrientation(quad, GLU_INSIDE)
        glPushMatrix()
        glTranslatef(self.x, self.y, self.z)
        glBindTexture(GL_TEXTURE_2D, self.image.gl_texture.texid)
        gluSphere(quad, self.radius, self.slices, self.stacks)
        glPopMatrix()

cdef class Sprite(VRDynamic):
    """
    """
    cdef float x
    cdef float y
    cdef float z
    cdef object texture
    cdef object image
    cdef float lowx
    cdef float highx
    cdef float lowy
    cdef float highy
    def __init__(self, float x, float y, float z, image, float xsize, float ysize):
        """
        """
        self.x = x
        self.y = y
        self.z = z
        self.image = image
        self.texture = image.gl_texture.texid
        self.highx = xsize / 2.0
        self.highy = ysize / 2.0
        lowx = -self.highx
        lowy = -self.highy
    def liveConstruct(self, float x, float y, float z, float yaw, float pitch, float roll, float fovy, float aspect, int width, int height):
        """
        """
        cdef object modelview
        cdef int i
        cdef int j
        glPushMatrix()
        glTranslatef(self.x, self.y, self.z)
        modelview = glGetFloatv(GL_MODELVIEW_MATRIX)
        if isinstance(modelview, list):
            modelview = asarray(modelview)
        for i from 0 <= i < 3:
            for j from 0 <= j < 3:
                if i == j:
                    modelview[i, j] = 1.0
                else:
                    modelview[i, j] = 0.0
        glLoadMatrixf(reshape(modelview, (16,)))
        glBindTexture(GL_TEXTURE_2D, self.texture)
        glBegin(GL_QUADS)
        glTexCoord2f(0.0, 1.0); glVertex2f(self.lowx, self.lowy)
        glTexCoord2f(1.0, 1.0); glVertex2f(self.highx, self.lowy)
        glTexCoord2f(1.0, 0.0); glVertex2f(self.highx, self.highy)
        glTexCoord2f(0.0, 0.0); glVertex2f(self.lowx, self.highy)
        glEnd()
        glPopMatrix()

cdef gridQuad(
    float Ax, float Ay, float Az, float Au, float Av,
    float Bx, float By, float Bz, float Bu, float Bv,
    float Cx, float Cy, float Cz, float Cu, float Cv, 
    float Dx, float Dy, float Dz, float Du, float Dv,
    float maxfacet):
        cdef float abLength
        cdef float bcLength
        cdef float cdLength
        cdef float daLength
        cdef int abFacets
        cdef int bcFacets
        cdef int abFacet
        cdef int bcFacet
        cdef float prop
        cdef float comp
        cdef float ab_cd_1a_x
        cdef float ab_cd_1a_y
        cdef float ab_cd_1a_z
        cdef float ab_cd_1a_u
        cdef float ab_cd_1a_v
        cdef float ab_cd_1b_x
        cdef float ab_cd_1b_y
        cdef float ab_cd_1b_z
        cdef float ab_cd_1b_u
        cdef float ab_cd_1b_v
        cdef float ab_cd_2a_x
        cdef float ab_cd_2a_y
        cdef float ab_cd_2a_z
        cdef float ab_cd_2a_u
        cdef float ab_cd_2a_v
        cdef float ab_cd_2b_x
        cdef float ab_cd_2b_y
        cdef float ab_cd_2b_z
        cdef float ab_cd_2b_u
        cdef float ab_cd_2b_v
        cdef float subAx
        cdef float subAy
        cdef float subAz
        cdef float subAu
        cdef float subAv
        cdef float subBx
        cdef float subBy
        cdef float subBz
        cdef float subBu
        cdef float subBv
        cdef float subCx
        cdef float subCy
        cdef float subCz
        cdef float subCu
        cdef float subCv
        cdef float subDx
        cdef float subDy
        cdef float subDz
        cdef float subDu
        cdef float subDv
        
        if maxfacet:
            abLength = sqrt((Bx - Ax) ** 2 + (By - Ay) ** 2 + (Bz - Az) ** 2)
            bcLength = sqrt((Cx - Bx) ** 2 + (Cy - By) ** 2 + (Cz - Bz) ** 2)
            cdLength = sqrt((Dx - Cx) ** 2 + (Dy - Cy) ** 2 + (Dz - Cz) ** 2)
            daLength = sqrt((Ax - Dx) ** 2 + (Ay - Dy) ** 2 + (Az - Dz) ** 2)
            abFacets = int(ceil(max(abLength, cdLength) / maxfacet))
            bcFacets = int(ceil(max(bcLength, daLength) / maxfacet))
        else:
            abFacets = 1
            bcFacets = 1
        for abFacet from 0 <= abFacet < abFacets:
            
            prop = abFacet / float(abFacets)
            comp = 1.0 - prop
            
            ab_cd_1a_x = Ax * prop + Bx * comp
            ab_cd_1a_y = Ay * prop + By * comp
            ab_cd_1a_z = Az * prop + Bz * comp
            ab_cd_1a_u = Au * prop + Bu * comp
            ab_cd_1a_v = Av * prop + Bv * comp
            
            ab_cd_1b_x = Dx * prop + Cx * comp
            ab_cd_1b_y = Dy * prop + Cy * comp
            ab_cd_1b_z = Dz * prop + Cz * comp
            ab_cd_1b_u = Du * prop + Cu * comp
            ab_cd_1b_v = Dv * prop + Cv * comp
            
            prop = (abFacet + 1) / float(abFacets)
            comp = 1.0 - prop
            
            ab_cd_2a_x = Ax * prop + Bx * comp
            ab_cd_2a_y = Ay * prop + By * comp
            ab_cd_2a_z = Az * prop + Bz * comp
            ab_cd_2a_u = Au * prop + Bu * comp
            ab_cd_2a_v = Av * prop + Bv * comp
            
            ab_cd_2b_x = Dx * prop + Cx * comp
            ab_cd_2b_y = Dy * prop + Cy * comp
            ab_cd_2b_z = Dz * prop + Cz * comp
            ab_cd_2b_u = Du * prop + Cu * comp
            ab_cd_2b_v = Dv * prop + Cv * comp
            
            for bcFacet from 0 <= bcFacet < bcFacets:
                prop = bcFacet / float(bcFacets)
                comp = 1.0 - prop
                
                subAx = ab_cd_1a_x * prop + ab_cd_1b_x * comp
                subAy = ab_cd_1a_y * prop + ab_cd_1b_y * comp
                subAz = ab_cd_1a_z * prop + ab_cd_1b_z * comp
                subAu = ab_cd_1a_u * prop + ab_cd_1b_u * comp
                subAv = ab_cd_1a_v * prop + ab_cd_1b_v * comp
                
                subBx = ab_cd_2a_x * prop + ab_cd_2b_x * comp
                subBy = ab_cd_2a_y * prop + ab_cd_2b_y * comp
                subBz = ab_cd_2a_z * prop + ab_cd_2b_z * comp
                subBu = ab_cd_2a_u * prop + ab_cd_2b_u * comp
                subBv = ab_cd_2a_v * prop + ab_cd_2b_v * comp
                
                prop = (bcFacet + 1) / float(bcFacets)
                comp = 1.0 - prop
                
                subDx = ab_cd_1a_x * prop + ab_cd_1b_x * comp
                subDy = ab_cd_1a_y * prop + ab_cd_1b_y * comp
                subDz = ab_cd_1a_z * prop + ab_cd_1b_z * comp
                subDu = ab_cd_1a_u * prop + ab_cd_1b_u * comp
                subDv = ab_cd_1a_v * prop + ab_cd_1b_v * comp
                
                subCx = ab_cd_2a_x * prop + ab_cd_2b_x * comp
                subCy = ab_cd_2a_y * prop + ab_cd_2b_y * comp
                subCz = ab_cd_2a_z * prop + ab_cd_2b_z * comp
                subCu = ab_cd_2a_u * prop + ab_cd_2b_u * comp
                subCv = ab_cd_2a_v * prop + ab_cd_2b_v * comp
                
                glTexCoord2f(subAu, subAv); glVertex3f(subAx, subAy, subAz)
                glTexCoord2f(subBu, subBv); glVertex3f(subBx, subBy, subBz)
                glTexCoord2f(subCu, subCv); glVertex3f(subCx, subCy, subCz)
                glTexCoord2f(subDu, subDv); glVertex3f(subDx, subDy, subDz)

cdef class SkyBox(VRVisible):
    """
    """
    cdef float x
    cdef float y
    cdef float z
    cdef float xsize
    cdef float ysize
    cdef float zsize
    cdef object imageFront
    cdef object imageRear
    cdef object imageLeft
    cdef object imageRight
    cdef float xwalltiletimesFront
    cdef float ywalltiletimesFront
    cdef float xwalltiletimesRear
    cdef float ywalltiletimesRear
    cdef float xwalltiletimesLeft
    cdef float ywalltiletimesLeft
    cdef float xwalltiletimesRight
    cdef float ywalltiletimesRight

    def __init__(self, imageFront, imageRear, imageLeft, imageRight, x = 0.0, y = 0.0, z = 0.0, xsize = 500.0, ysize = 500.0, zsize = 500.0, texlenFront = 500.0, texlenRear = 500.0, texlenLeft = 500.0, texlenRight = 500.0, xTileFactorFront = 1, xTileFactorRear = 1, xTileFactorLeft = 1, xTileFactorRight = 1, yTileFactorFront = 1, yTileFactorRear = 1, yTileFactorLeft = 1, yTileFactorRight = 1):
        """
        """
        self.x = x
        self.y = y
        self.z = z
        self.xsize = xsize
        self.ysize = ysize
        self.zsize = zsize
        self.imageFront = imageFront
        self.imageRear = imageRear
        self.imageLeft = imageLeft
        self.imageRight = imageRight
        self.xwalltiletimesFront = (xsize / texlenFront)*xTileFactorFront
        self.ywalltiletimesFront = (ysize / texlenFront)*yTileFactorFront
        self.xwalltiletimesRear = (xsize / texlenRear)*xTileFactorRear
        self.ywalltiletimesRear = (ysize / texlenRear)*yTileFactorRear
        self.xwalltiletimesLeft = (xsize / texlenLeft)*xTileFactorLeft
        self.ywalltiletimesLeft = (ysize / texlenLeft)*yTileFactorLeft
        self.xwalltiletimesRight = (xsize / texlenRight)*xTileFactorRight
        self.ywalltiletimesRight = (ysize / texlenRight)*yTileFactorRight
    def construct(self):
        global maxfacetlength
        cdef float lowx
        cdef float highx
        cdef float lowy
        cdef float highy
        cdef float lowz
        cdef float highz
        cdef float halfxsize
        cdef float halfzsize
        halfxsize = self.xsize / 2.0
        halfzsize = self.zsize / 2.0
        lowx = self.x - halfxsize
        highx = self.x + halfxsize
        lowy = self.y
        highy = self.y + self.ysize
        lowz = self.z - halfzsize
        highz = self.z + halfzsize
       
        # don't use border color when filtering texture edges
        GL_CLAMP_TO_EDGE = 0x812F

        # Walls...        
        # Front face
        glBindTexture(GL_TEXTURE_2D, self.imageFront.gl_texture.texid)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glBegin(GL_QUADS)
        gridQuad(
            highx, highy, lowz, 0, 0,
            lowx, highy, lowz, self.xwalltiletimesFront, 0,
            lowx, lowy, lowz, self.xwalltiletimesFront, self.ywalltiletimesFront,
            highx, lowy, lowz, 0, self.ywalltiletimesFront,
            maxfacetlength)
        glEnd()
        
        # Rear face
        glBindTexture(GL_TEXTURE_2D, self.imageRear.gl_texture.texid)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glBegin(GL_QUADS)
        gridQuad(
            lowx, highy, highz, 0, 0,
            highx, highy, highz, self.xwalltiletimesRear, 0,
            highx, lowy, highz, self.xwalltiletimesRear, self.ywalltiletimesRear,
            lowx, lowy, highz, 0, self.ywalltiletimesRear,
            maxfacetlength)
        glEnd()
        
        # Left face
        glBindTexture(GL_TEXTURE_2D, self.imageLeft.gl_texture.texid)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glBegin(GL_QUADS)
        gridQuad(
            lowx, highy, lowz, 0, 0,
            lowx, highy, highz, self.xwalltiletimesLeft, 0,
            lowx, lowy, highz, self.xwalltiletimesLeft, self.ywalltiletimesLeft,
            lowx, lowy, lowz, 0, self.ywalltiletimesLeft,
            maxfacetlength)
        glEnd()
        
        # Right face
        glBindTexture(GL_TEXTURE_2D, self.imageRight.gl_texture.texid)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        glBegin(GL_QUADS)
        gridQuad(
            highx, highy, highz, 0, 0,
            highx, highy, lowz, self.xwalltiletimesRight, 0,
            highx, lowy, lowz, self.xwalltiletimesRight, self.ywalltiletimesRight,
            highx, lowy, highz, 0, self.ywalltiletimesRight,
            maxfacetlength)
        glEnd()

cdef class FloorBox(VRVisible):
    """
    """
    cdef float x
    cdef float y
    cdef float z
    cdef float xsize
    cdef float ysize
    cdef float zsize
    cdef object floorimage
    cdef float xfloortiletimes
    cdef float yfloortiletimes
    cdef object wallimageFront
    cdef object wallimageRear
    cdef object wallimageLeft
    cdef object wallimageRight
    cdef float xwallFrontTiletimes
    cdef float ywallFrontTiletimes
    cdef float xwallRearTiletimes
    cdef float ywallRearTiletimes
    cdef float xwallLeftTiletimes
    cdef float ywallLeftTiletimes
    cdef float xwallRightTiletimes
    cdef float ywallRightTiletimes
 
    def __init__(self, x, y, z, xsize, ysize, zsize, floorimage, floortexlen = None, wallimageFront = None, walltexlenFront = None, wallimageRear = None, walltexlenRear = None, wallimageLeft = None, walltexlenLeft = None, wallimageRight = None, walltexlenRight = None):
        """
        """
        self.x = x
        self.y = y
        self.z = z
        self.xsize = xsize
        self.ysize = ysize
        self.zsize = zsize
        self.floorimage = floorimage
        if floortexlen:
            self.xfloortiletimes = xsize / floortexlen
            self.yfloortiletimes = zsize / floortexlen
        else:
            self.xfloortiletimes = 1.0
            self.yfloortiletimes = 1.0
        self.wallimageFront = wallimageFront
        self.wallimageRear = wallimageRear
        self.wallimageLeft = wallimageLeft
        self.wallimageRight = wallimageRight
        
        self.xwallFrontTiletimes = 1.0
        self.ywallFrontTiletimes = 1.0
        self.xwallRearTiletimes = 1.0
        self.ywallRearTiletimes = 1.0
        self.xwallLeftTiletimes = 1.0
        self.ywallLeftTiletimes = 1.0
        self.xwallRightTiletimes = 1.0
        self.ywallRightTiletimes = 1.0
        
        if walltexlenFront:
            self.xwallFrontTiletimes = (xsize / walltexlenFront)#/14.5
            self.ywallFrontTiletimes = ysize / walltexlenFront
        if walltexlenRear:
            self.xwallRearTiletimes = (xsize / walltexlenRear)#/14.5
            self.ywallRearTiletimes = ysize / walltexlenRear
        if walltexlenLeft:
            self.xwallLeftTiletimes = (xsize / walltexlenLeft)#/14.5
            self.ywallLeftTiletimes = ysize / walltexlenLeft
        if walltexlenRight:
            self.xwallRightTiletimes = (xsize / walltexlenRight)#/14.5
            self.ywallRightTiletimes = ysize / walltexlenRight

    def construct(self):
        global maxfacetlength
        cdef float lowx
        cdef float highx
        cdef float lowy
        cdef float highy
        cdef float lowz
        cdef float highz
        cdef float halfxsize
        cdef float halfzsize
        cdef int x
        cdef int y
        halfxsize = self.xsize / 2.0
        halfzsize = self.zsize / 2.0
        lowx = self.x - halfxsize
        highx = self.x + halfxsize
        lowy = self.y
        highy = self.y + self.ysize
        lowz = self.z - halfzsize
        highz = self.z + halfzsize
        
        # Floor...
        glBindTexture(GL_TEXTURE_2D, self.floorimage.gl_texture.texid)
        glBegin(GL_QUADS)
        gridQuad(
            lowx, lowy, highz, 0, 0,
            highx, lowy, highz, self.xfloortiletimes, 0,
            highx, lowy, lowz, self.xfloortiletimes, self.yfloortiletimes,
            lowx, lowy, lowz, 0, self.yfloortiletimes,
            maxfacetlength)
        glEnd()
        
        # Walls...
        if self.wallimageFront:
            # Front face
            glBindTexture(GL_TEXTURE_2D, self.wallimageFront.gl_texture.texid)
            glBegin(GL_QUADS)
            gridQuad(
                highx, highy, lowz, 0, 0,
                lowx, highy, lowz, self.xwallFrontTiletimes, 0,
                lowx, lowy, lowz, self.xwallFrontTiletimes, self.ywallFrontTiletimes,
                highx, lowy, lowz, 0, self.ywallFrontTiletimes,
                maxfacetlength)
        if self.wallimageRear:
            # Rear face
            glEnd()
            glBindTexture(GL_TEXTURE_2D, self.wallimageRear.gl_texture.texid)
            glBegin(GL_QUADS)
            gridQuad(
                lowx, highy, highz, 0, 0,
                highx, highy, highz, self.xwallRearTiletimes, 0,
                highx, lowy, highz, self.xwallRearTiletimes, self.ywallRearTiletimes,
                lowx, lowy, highz, 0, self.ywallRearTiletimes,
                maxfacetlength)
            glEnd()
        if self.wallimageLeft:
            glBindTexture(GL_TEXTURE_2D, self.wallimageLeft.gl_texture.texid)
            glBegin(GL_QUADS)
            # Left face
            gridQuad(
                lowx, highy, lowz, 0, 0,
                lowx, highy, highz, self.xwallLeftTiletimes, 0,
                lowx, lowy, highz, self.xwallLeftTiletimes, self.ywallLeftTiletimes,
                lowx, lowy, lowz, 0, self.ywallLeftTiletimes,
                maxfacetlength)
            # Right face
            glEnd()
        if self.wallimageRight:
            glBindTexture(GL_TEXTURE_2D, self.wallimageRight.gl_texture.texid)
            glBegin(GL_QUADS)
            gridQuad(
                highx, highy, highz, 0, 0,
                highx, highy, lowz, self.xwallRightTiletimes, 0,
                highx, lowy, lowz, self.xwallRightTiletimes, self.ywallRightTiletimes,
                highx, lowy, highz, 0, self.ywallRightTiletimes,
                maxfacetlength)
            glEnd()

cdef class BuildingBox(VRVisible):
    """
    """
    cdef float x
    cdef float y
    cdef float z
    cdef object image
    cdef float width
    cdef float height
    cdef object roofimage
    cdef float xrooftiletimes
    cdef float xtiletimes
    cdef float yrooftiletimes
    cdef float ytiletimes
    def __init__(self, x, y, z, image, width, height, roofimage = None, rooftexlen = None, texlen = None):
        """
        """
        self.x = x
        self.y = y
        self.z = z
        self.image = image
        self.width = width
        self.height = height
        self.roofimage = roofimage
        if rooftexlen:
            self.xrooftiletimes = width / rooftexlen
            self.yrooftiletimes = width / rooftexlen
        else:
            self.xrooftiletimes = 1.0
            self.yrooftiletimes = 1.0
        if texlen:
            self.xtiletimes = height / texlen
            self.ytiletimes = height / texlen
        else:
            self.xtiletimes = 1.0
            self.ytiletimes = 1.0
    def construct(self):
        cdef float lowx
        cdef float highx
        cdef float lowy
        cdef float highy
        cdef float lowz
        cdef float highz
        cdef float halfwidth
        halfwidth = self.width / 2.0
        lowx = self.x - halfwidth
        highx = self.x + halfwidth
        lowy = self.y
        highy = self.y + self.height
        lowz = self.z - halfwidth
        highz = self.z + halfwidth
        
        # Walls...
        glBindTexture(GL_TEXTURE_2D, self.image.gl_texture.texid)
        glBegin(GL_QUADS)
        
        # Front face
        gridQuad(
            lowx, highy, lowz, self.xtiletimes, 0,
            highx, highy, lowz, 0, 0,
            highx, lowy, lowz, 0, self.ytiletimes,
            lowx, lowy, lowz, self.xtiletimes, self.ytiletimes,
            maxfacetlength)
        
        # Rear face
        gridQuad(
            highx, highy, highz, self.xtiletimes, 0,
            lowx, highy, highz, 0, 0,
            lowx, lowy, highz, 0, self.ytiletimes,
            highx, lowy, highz, self.xtiletimes, self.ytiletimes,
            maxfacetlength)
        
        # Left face
        gridQuad(
            lowx, highy, highz, self.xtiletimes, 0,
            lowx, highy, lowz, 0, 0,
            lowx, lowy, lowz, 0, self.ytiletimes,
            lowx, lowy, highz, self.xtiletimes, self.ytiletimes,
            maxfacetlength)
        
        # Right face
        gridQuad(
            highx, highy, lowz, self.xtiletimes, 0,
            highx, highy, highz, 0, 0,
            highx, lowy, highz, 0, self.ytiletimes,
            highx, lowy, lowz, self.xtiletimes, self.ytiletimes,
            maxfacetlength)
        glEnd()
        
        # Roof...
        if self.roofimage:
            glBindTexture(GL_TEXTURE_2D, self.roofimage.gl_texture.texid)
            glBegin(GL_QUADS)
            gridQuad(
                lowx, highy, highz, 0, 0,
                highx, highy, highz, self.xrooftiletimes, 0,
                highx, highy, lowz, self.xrooftiletimes, self.yrooftiletimes,
                lowx, highy, lowz, 0, self.yrooftiletimes,
                maxfacetlength)
            glEnd()
