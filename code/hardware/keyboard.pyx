# PyEPL: hardware/keyboard.pyx
#
# Copyright (C) 2003-2005 Michael J. Kahana
# Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
# URL: http://memory.psych.upenn.edu/programming/pyepl
#
# Distributed under the terms of the GNU Lesser General Public License
# (LGPL). See the license.txt that came with this file.

"""
This module provides low-level keyboard functionality.
"""

import pygame
import eventpoll
import sys

def initialize(**options):
    """
    Do any preparation necessary before using keyboard input.
    """
    global nametokeydict
    global keytonamedict
    nametokeydict = {}  # dictionary mapping key names to identifiers that can be used with keyboard functions
    keytonamedict = {}  # ...opposite
    for x in xrange(512):
        name = pygame.key.name(x).upper()
        nametokeydict[name] = x
        keytonamedict[x] = name
##     if sys.platform == "darwin":
##         nametokeydict["RETURN"] = nametokeydict["ENTER"]
##     else:
##         nametokeydict["ENTER"] = nametokeydict["RETURN"]

def finalize():
    """
    Shut down keyboard features.
    """
    setKeyboardCallback(None)

def nameToKey(name):
    """
    Returns the key identifier for the given name.
    """
    global nametokeydict
    return nametokeydict[name.upper()]

def keyToName(k):
    """
    Returns the name for the given key identifier.
    """
    global keytonamedict
    return keytonamedict[k]

def keyNames():
    """
    Returns an iterator over the names of all the available keys.
    """
    global nametokeydict
    return nametokeydict.iterkeys()

setKeyboardCallback = eventpoll.setKeyboardCallback
