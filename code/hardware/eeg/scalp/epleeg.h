// PyEPL: hardware/eeg/scalp/epleeg.h
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

#ifndef __EPLEEG_H
#define __EPLEEG_H

#include "mbuff.h"

#include <sys/ipc.h>
#include <sys/shm.h>

#include <fstream>
#include <iostream>

#include "shmem_struct.h"
#include "rt_broker_module.h"

using namespace std;


// Defines for error catching
#define ERROR_MBUFF_ATTACH -1
#define ERROR_SHMEM_ATTACH -2
#define ERROR_SHMEM_ACCESS -3
#define ERROR_SHMEM_MAGIC -4


class EEG
{
 public:
  EEG();
  ~EEG();
  int Attached();
  int ShmAttach();
  int ShmDetach();
  int RecStart(char *filename);
  int RecStop();
  long GetOffset();
  
 private:
  struct shared_use_st *share;
  struct SharedMem *rt_shm;
  unsigned long long start_rec_time;

};

#endif
