// PyEPL: hardware/eeg/scalp/rt_broker_module.h
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

#ifndef _RT_BROKER_MODULE_H
#define _RT_BROKER_MODULE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "cardinfo.h"


#define DEFAULT_RANGE 1 /* [-5,5] range */
#define DEFAULT_AREF AREF_GROUND


#define MAX_SAMPLES 1000

#define DEFAULT_SAMPLING_RATE_HZ SR

#define DEFAULT_FIFO_MINOR     0  // /dev/rtf0
#define DEFAULT_FIFO_SIZE      (MAX_SAMPLES * sizeof(sampl_t) * CH_PER_CARD)



#define STR_(x) #x
#define STR(x) "" STR_(x) ""

#ifdef __KERNEL__
#define const_
#else
#define const_ const
#endif

#define SHARED_MEM_MAGIC (0xdeadbeef) // Mmmmm, delicious

struct SharedMem {
  const_ unsigned int ai_subdev;
  const_ unsigned long long scan_index;
  const_ int magic;
};

#define SHARED_MEM_NAME "Shared Memory Jubba Jubba"


#ifdef __cplusplus
}
#endif

#endif
