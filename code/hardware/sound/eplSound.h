// PyEPL: hardware/sound/eplSound.h
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.
/*
  - Allow for resampling on append to fifo.
  - Separate recstart and playstart
*/


#ifndef __EPLSOUND_H
#define __EPLSOUND_H

#include <iostream>
#include "RtAudio.h"
#include "MyType.h"
#include "fifo.h"

using namespace std;

class audioBuffer
{
 public:
  audioBuffer(long recLen, long playLen, int inRecChans, int inPlayChans, int samprate);
  ~audioBuffer();
  fifo *recBuf;
  fifo *playBuf;
  int recChans;
  int playChans;
  int rate;

  int recording;
  long samplesPlayed;
};

#define FORMAT RTAUDIO_SINT16

class eplSound 
{
 public:
  eplSound(long recLen=REC_BUF_LEN, long playLen=PLAY_BUF_LEN, int sampleRate=SAMPLE_RATE, 
	   int bufSize=BUF_SIZE);
  ~eplSound();
  long append(MY_TYPE *newdata, long length, int overwrite, float ampFactor);
  long consume(MY_TYPE *newdata, long length);
  void clear();
  int recstart();
  int recstop();
  int startstream();
  int stopstream();
  int getBufferSize();
  long getSamplesPlayed();
  void resetSamplesPlayed();
  int getRecChans();
  int getPlayChans();
  int getSampleRate();
  long getBufferUsed();
  static const int SCALE=32767;  
  static const int SAMPLE_RATE=44100;
  static const int SAMPLE_SILENCE=0;
  static const int BUF_SIZE=256;
  static const int NUM_INTERNAL_BUFF=4;
  static const int DEVICE=0;
  static const int FORMAT_SIZE=2;
  static const int PLAY_BUF_LEN=60;
  static const int REC_BUF_LEN=60;
  static const int NUM_CHANNELS=2;
 private:
  RtAudio *audio;
  RtAudio *playaudio;
  RtAudio *recaudio;
  int isDuplex;
  int playChans;
  int recChans;
  int rate;
  int bufferSize;
  int playDevice;
  int recDevice;
  int sampleRate;
  int streaming;
  audioBuffer *data;
};

// audio callback function
int inout(char *buffer, int buffer_size, void *data);
int playcall(char *buffer, int buffer_size, void *data);
int reccall(char *buffer, int buffer_size, void *data);

#endif
