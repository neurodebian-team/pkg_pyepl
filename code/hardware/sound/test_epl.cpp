// PyEPL: hardware/sound/test_epl.cpp
//
// Copyright (C) 2003-2005 Michael J. Kahana
// Authors: Ian Schleifer, Per Sederberg, Aaron Geller, Josh Jacobs
// URL: http://memory.psych.upenn.edu/programming/pyepl
//
// Distributed under the terms of the GNU Lesser General Public License
// (LGPL). See the license.txt that came with this file.

#include <iostream>
using std::cout;
using std::endl;


int main(int argc, char *argv[])
{

  int maxsecs = 60;
  int secs = 10;
  int chans = 2;
  int rate = 44100;
  long buflen = secs*chans*rate;
  long maxbuflen = maxsecs*chans*rate;
  eplSound *sound = new eplSound(maxsecs,maxsecs,rate);
  
  MY_TYPE *data =  new MY_TYPE[buflen];

  int i;
  long consumed;

  sound->startstream();

  sound->recstart();

  cout << "recording...\n";

  sleep(secs);

  sound->recstop();

  consumed = sound->consume(data,buflen);

  cout << consumed << endl;

  for (i=0;i<20;i++)
  {
    sound->append(data,consumed,0);
    
    cout << "playing ... \n";
    
    sleep(secs);
  }

  sound->stopstream();

  delete sound;
  delete[] data;


}


