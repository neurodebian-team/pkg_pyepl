//
// Fifo class using the STL queue
//



#ifndef FIFO_H
#define FIFO_H

#include "MyType.h"
#include <string>

using namespace std;

class fifo
{
 public:
  fifo(long length);
  ~fifo();
  long append(MY_TYPE *newdata, long length, int overwrite = 1);
  long consume(MY_TYPE *newdata, long length);
  void clear();
  long getUsed();
  
 private:
  MY_TYPE *data;
  long used;
  long capacity;
  long startInd;
  long endInd;
  int isFull;
};



#endif
