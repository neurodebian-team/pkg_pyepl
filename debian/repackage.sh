#!/bin/bash
#-------------------------- =+- Shell script -+= --------------------------
#
# @file      repackage.sh
# @date      Tue Jun 20 12:13:04 2006
#
#  Yaroslav Halchenko                                      CS@UNM, CS@NJIT
#  web:     http://www.onerussian.com                      & PSYCH@RUTGERS
#  e-mail:  debian@onerussian.com                           ICQ#: 60653192
#
# DESCRIPTION (NOTES):
#
#   Simple script to repackage upstream removing unwanted material and
#   then feeding to specified command. Just to automate uscan invocation
#   of svn-upgrade... lazy me I know
#
#-----------------\____________________________________/------------------
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# SVN version control block - do not edit manually
RPT_SVN_ID='$Id:  $'
RPT_SVN_REV='$Rev: 1 $'
RPT_SVN_DATE='$Date:  $'
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###

############
# Defaults #
############

# repackage version
rpt_version=0.1.${RPT_SVN_REV//[A-Za-z :\$]/}

delete_files=''
post_command=

upstream_version=

#suffixes
suffix_pristine=pristine
suffix_out=dfsg
suffix_version_out=.${suffix_out}.1

print_help()
{
print_version
cat << EOT

Repackage Debian packages by removing specified unwanted material
(usually Non-DFSG compliant) and calling specified command (if any).

Usage:
  repackage [OPTIONS] <package>

  Where <package> is a tarball fetched from online.

Options:
  -h, --help
    Print this help and exit.

  -V, --version
    Print version information and exit.

  -d <files>, --delete-files <files>
    Space separated list of files to be removed.

  -c <command>, --command <command>
    Command to execute on generated file.

  --upstream-version <version>
    Just to pass to command
EOT
}

print_version()
{
cat << EOT
repackage $rpt_version
Copyright (C) 2006 Yaroslav Halchenko <debian@onerussian.com>
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

EOT
}

################################
# Commandline options handling #
################################

# Parse commandline options (taken from the getopt examples from the
# Debian util-linux package).
# Note that we use `"$@"' to let each command-line parameter expand to
# a separate word. The quotes around `$@' are essential!  We need
# CLOPTS as the `eval set --' would nuke the return value of getopt.

CLOPTS=`getopt -o h,V,d:,c: --long help,version,upstream-version:,delete-files:,command: -n '$0' -- "$@"`

if [ $? != 0 ] ; then
    echo "Terminating..." >&2
    exit 1
fi

# Note the quotes around `$CLOPTS': they are essential!
eval set -- "$CLOPTS"

while true ; do
    case "$1" in
        --version) print_version; exit 0;;
        -c|--command) shift; post_command=$1; shift;;
        -d|--delete-files) shift; delete_files="$1 $delete_files"; shift;;
        --upstream-version) shift; upstream_version="$1"; shift;;
        --) shift ; break ;;
        *) echo "Internal error! ($1)"; exit 1;;
    esac
done

if [ $# -lt 1 ]; then
    printf "Too few arguments: $@.\n\n"
    print_help
    exit 1
fi

if [ $# -gt 1 ]; then
    printf "Too many arguments: $@.\n\n"
    print_help
    exit 1
fi

if [ -z "$delete_files" ]; then
        echo "No files to delete were provided, assuming that it is called by uscan: sourcing debian/repackage.params"
        [ -f debian/repackage.params ] && source debian/repackage.params
fi

fname="$1"
if [ ! -f $fname ]; then
	echo "Can't read $fname"
	exit 1
fi

full_fname=`readlink -f $1`

# if there are files to delete -- repackage, otherwise keep original file
if [ ! -z "$delete_files" ]; then
	pristine_full_fname="${full_fname%t[argz.]*}${suffix_pristine}.tar.gz"
	output_full_fname="${full_fname%t[argz.]*}${suffix_out}.tar.gz"

    # move original (pristine) tarball aside
	mv "$full_fname" "$pristine_full_fname"

	zcat $pristine_full_fname \
		| tar --delete $delete_files | gzip -9 \
		>| $output_full_fname
else
	output_full_fname="$full_fname"
	suffix_version_out=""
fi

# Run post_command if any was assigned
if [ ! -z "$post_command" ]; then
	params=""
	[ -z "$upstream_version" ] || params="--upstream-version $upstream_version$suffix_version_out"
	$post_command $params $output_full_fname
fi
