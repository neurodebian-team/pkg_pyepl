#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
#
# This file was originally written by Joey Hess and Craig Small.
# build-arch and build-indep targets  by Bill Allombert 2001
# Changes for python-pyepl by Yaroslav Halchenko

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This has to be exported to make some magic below work.
export DH_OPTIONS

# no parallel execution -- required for dpatch
.NOTPARALLEL:

PYVERS  := $(shell pyversions -vs)
PYVER   := $(shell pyversions -vd)

PYNAME=pyepl
PYPACKAGE=python-$(PYNAME)
BASE=$(CURDIR)/debian
PY_COMMON_PATH=$(BASE)/$(PYPACKAGE)-common/usr/share/$(PYPACKAGE)/

CFLAGS = -Wall -g
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
else
	CFLAGS += -O2
endif

include /usr/share/dpatch/dpatch.make

#Architecture
build: debian/control patch build-arch build-indep
build-arch: build-arch-stamp
build-arch-stamp: $(PYVERS:%=build-arch-python%)
	touch $@

build-arch-python%:
	python$* setup.py build_ext
	touch $@

debian/control: debian/control.in
	sed -e "/#include \"description.in\"/r debian/description.in" -e "/#.*/d" $^ >| $@

build-indep: build-indep-stamp
build-indep-stamp:
#	$(MAKE) -C code/documentation/
	touch $@

clean: clean-patched unpatch
clean-patched: debian/control
	dh_testdir
	dh_testroot

	# proper cleaning is available only for patched sources
	if [ -f patch-stamp ]; then python setup.py clean; fi

	rm -f build-arch-stamp build-arch-python* build-indep-stamp
	rm -rf build
	dh_clean

pre-install:
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

install: build pre-install install-indep install-arch
install-indep:
	dh_installdirs -i

	mkdir -p $(PY_COMMON_PATH)
	cp -rp code/resources/*.png $(PY_COMMON_PATH)

	dh_install -i

install-python%:
	python$* setup.py install --root $(BASE)/$(PYPACKAGE) --no-compile
	python$* setup.py clean

install-arch: pre-install $(PYVERS:%=install-python%)

# Must not depend on anything. This is to be called by
# binary-arch/binary-indep
# in another 'make' thread.
binary-common:
	dh_testdir
	dh_testroot
	dh_installchangelogs
	dh_installdocs
	dh_link

	dh_strip

	dh_pycentral
	dh_python

	# Need to create links semi-automatically after call to pycentral,
	# otherwise they will be ruined. And provide condition to work
	# only for arch-dep packages
	if [ "$(DH_OPTIONS)" = "-a" ]; then \
	for py in $(PYVERS); do \
		dh_link -a  usr/share/$(PYPACKAGE) \
				 usr/lib/python$$py/site-packages/$(PYNAME)/resources; \
	done; \
	fi

	dh_compress -X.py -X.pdf
	dh_fixperms
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

# Build architecture independant packages using the common target.
binary-indep: build-indep install-indep
	$(MAKE) -f debian/rules DH_OPTIONS=-i binary-common

# Build architecture dependant packages using the common target.
binary-arch: build-arch install-arch
	$(MAKE) -f debian/rules DH_OPTIONS=-a binary-common

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install install-indep install-arch
