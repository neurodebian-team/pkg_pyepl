#!/usr/bin/python

from distutils.core import setup, Extension
from distutils.sysconfig import get_config_var
from Pyrex.Distutils import build_ext 
import os
import sys

ext_modules=[]

platform = os.uname()[0]

BUILD_AWCARD = False
BUILD_PARALLEL = False

if platform=='Darwin':
    ext_modules.append(
	Extension(name='pyepl.hardware.graphics.screensync._refreshBlock',
		  sources=['code/hardware/graphics/screensync/refreshBlock.m'],
		  extra_link_args=['-framework', 'OpenGL'])
	)
    BUILD_AWCARD = True

if platform=='Linux':
    BUILD_PARALLEL=True

# set up pyrex extension modules:
# hardware modules
ext_modules.append(Extension(name='pyepl.hardware.timing', sources=['code/hardware/timing.pyx']))
ext_modules.append(Extension(name='pyepl.hardware.eventpoll', sources=['code/hardware/eventpoll.pyx']))
ext_modules.append(Extension(name='pyepl.hardware.joystick', sources=['code/hardware/joystick.pyx']))
ext_modules.append(Extension(name='pyepl.hardware.keyboard', sources=['code/hardware/keyboard.pyx']))
ext_modules.append(Extension(name='pyepl.hardware.mouse', sources=['code/hardware/mouse.pyx']))
# hardware.eeg modules
if platform=='Linux':
    ext_modules.append(Extension(name='pyepl.hardware.eeg.pulse.pulse', sources=['code/hardware/eeg/pulse/eplPulse.c', 
										 'code/hardware/eeg/pulse/pulse.pyx']))
ext_modules.append(Extension(name='pyepl.hardware.eeg.scalp.scalp', 
			     sources=['code/hardware/eeg/scalp/scalp.pyx', 'code/hardware/eeg/scalp/epleeg.cpp', 
				      'code/hardware/eeg/scalp/epleeg_wrapper.cpp']))
# hardware.vr modules
if platform=='Darwin' and sys.version[:3]=='2.4':
    # currently distutils for Python 2.4 isn't looking under /usr/local,
    # necessitating this 
    INC_DIR = '/usr/local/include'
    LIB_DIR = '/usr/local/lib'
    ext_modules.append(Extension(name='pyepl.hardware.vr.avatar', sources=['code/hardware/vr/avatar.pyx'],
				 include_dirs=[INC_DIR], libraries=['ode'], library_dirs=[LIB_DIR]))
else:
    ext_modules.append(Extension(name='pyepl.hardware.vr.avatar', sources=['code/hardware/vr/avatar.pyx'],
				 libraries=['ode']))
ext_modules.append(Extension(name='pyepl.hardware.vr.environment', sources=['code/hardware/vr/environment.pyx']))
ext_modules.append(Extension(name='pyepl.hardware.vr.eyes', sources=['code/hardware/vr/eyes.pyx']))

# These targets require SWIG and aren't handled (yet) by distutils
swig_targets = map(lambda x: os.path.join('hardware', x), 
		   ['sound', 'rt'])

if BUILD_AWCARD or BUILD_PARALLEL:
    swig_targets.append(os.path.join('hardware', 'eeg', 'pulse'))

for target in swig_targets: # call ye olde makefile
    res = os.system('make -C %s' % os.path.join('code', target))
    if res != 0:
	# if failed, stop
	print ""
	print "!!! Compilation Error !!!"
	print "Make failed for target %s" % target
	print "Exiting...Please fix the above error and try again..."
	print ""
	sys.exit(res)

site_packages_dir = os.path.join(get_config_var('BINLIBDEST'), 'site-packages')
data_files = []

data_files.append((os.path.join(site_packages_dir, 'pyepl', swig_targets[0]), 
		  map(lambda x: os.path.join('code', swig_targets[0], x), 
		      ['_eplSound.so', '_soundFile.so', 'RtAudio.h', 'RtAudio.cpp', 
		       'RtError.h', 'RtAudio_readme'])))

if platform=='Darwin': # we only have RT & ActiveWire installables for the mac
    data_files.append((os.path.join(site_packages_dir, 'pyepl', swig_targets[1]),
                       map(lambda x: os.path.join('code', swig_targets[1], x), 
                           ['_realtime.so'])))
    if BUILD_AWCARD: 
	data_files.append((os.path.join(site_packages_dir, 'pyepl', swig_targets[2]),
			   map(lambda x: os.path.join('code', swig_targets[2], x), 
			       ['_awCard.so'])))

if BUILD_PARALLEL:
    data_files.append((os.path.join(site_packages_dir, 'pyepl', swig_targets[2]),
                       map(lambda x: os.path.join('code', swig_targets[2], x), 
                           ['_parallel.so'])))
# License file:
data_files.append((os.path.join(site_packages_dir, 'pyepl'), 
		   ['code/license.txt']))
# Miscellaneous resources
data_files.append((os.path.join(os.path.join(site_packages_dir, 'pyepl'), 'resources'),
		   map(lambda x: os.path.join('code', 'resources', x), 
		       ['vera.ttf', 'icon.png', 'splash.png'])))

setup(name='pyepl', 
      version='1.0.29', ### MAKE SURE THIS MATCHES code/version.py !!
      package_dir={"pyepl":"code"},
      packages=['pyepl', 'pyepl.hardware', 'pyepl.hardware.eeg', 
		'pyepl.hardware.eeg.pulse', 'pyepl.hardware.eeg.scalp', 
		'pyepl.hardware.graphics', 'pyepl.hardware.graphics.screensync', 
		'pyepl.hardware.rt', 'pyepl.hardware.sound', 'pyepl.hardware.vr', 
		'pyepl.vr'],
      author=['Ian Schleifer, Per Sederberg, Aaron Geller, and Josh Jacobs'],
      maintainer=['Aaron Geller'],
      maintainer_email=['aaronsg@sas.upenn.edu'],
      url=['http://pyepl.sourceforge.net'],
      ext_modules=ext_modules, 
      cmdclass = {'build_ext': build_ext}, 
      data_files=data_files)

